var App = Ember.Application.create({
	LOG_TRANSITIONS: true,
});


App.ApplicationAdapter = DS.RESTAdapter.extend({
	//namespace: 'wordpress/claves/php'
	//namespace: 'tutoriales/claves/php'
        //namespace: 'claves/php'
	namespace: 'php'
});

App.Word = DS.Model.extend({
	txt: DS.attr()
});

App.IndexRoute = Ember.Route.extend({
	model: function() {
		return this.store.findAll('word');
	}
});



App.WordController = Ember.ObjectController.extend({
	txt : function(){return "hola"},
});



var equi = {
	'a' : ['4'],
	'e' : ['3'],
	'i' : ['1'],
	'o' : ['0'],
}

var consonats = ['b','c','d','f','g','h','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

function minMay(letra){
	//opcion = Math.floor((Math.random() * 3));
	return Math.random() < 0.5 ? letra.toUpperCase(): letra.toLowerCase();
}

sim =  ["-", '_', '.', ';', ':', '!', '*', '+', "-", " ", ""]
function randomSim(){		
	max = sim.length-2;
		return sim[Math.floor(Math.random()* max)]; //restar 2
}
/*
	tx: String/ texto to change
	sim: String/ This simbol is goint to replace the white space
	rand: Boolean/ if chage white space with random simbols
	elite: Boolean/ indicate if it's goint to use 3l1t3
	toUptoLo: Boolean/ change UpCase an LoCAse
*/
function toPass2(tx, sim, rand, elite, toUptoLo){
	newPass = "";
	for (var i = 0, len = tx.length; i < len; i++) {
		w = tx[i];
		x = equi[w];

		rb = Math.random() < 0.5; // random boolean
		newPass += (w == " " && rand) ? randomSim() :
				   (w == " ") ? sim :
                   (x && rb && elite && !toUptoLo)? x[0] :
                   (x && elite && !toUptoLo)? w :
				   (x && rb && elite)?   x[0]  : 
				   toUptoLo? minMay(w) : w ;
		
		//newPass +=  (x? x[0] : minMay(w));
	}	
	return newPass;	
}


function espacios2simbolos(texto, simbolo){
	return texto.replace(/\s/g, simbolo);
}


var ejemplo = "Este es un ejemplo";
var currentsim = 8;
var currentmix = false;
var ll = "";
var first_time = true;
var new_word = "";
App.IndexController = Ember.ArrayController.extend({
	words_number: 3,
	min: 0,
	max: 3,
	newPass: "",
	texto: ejemplo,
	elite: false,
	mM: false,
	mixx: false,
	littlebit: ll,
	simbols: [
		{sim: "Con espacio", id: 9},
		{sim: "Sin espacio", id: 10},
		{sim: "---", id: 8},
	    {sim: "__", id: 1},
	    {sim: "...", id: 2},
	    {sim: ";;;;", id: 3},
	    {sim: "::::", id: 4},
	    {sim: "!!!!", id: 5},
	    {sim: "****", id: 6},
	    {sim: "++++", id: 7},
  	],
  	currentSimbols: "r",
  	simTouch: function(){
  		return toPass2(this.texto, "", true, false, false) //tx, sim, rand, elite, toUptoLo
  	}.property('texto'),
	toUptoLo: function(){
		return toPass2(this.texto, " ", false, false, true) //tx, sim, rand, elite, toUptoLo
	}.property('texto'),
	ourSugestion: function(){
		new_word = toPass2(this.texto, " ", true, false, true)
		return new_word //tx, sim, rand, elite, toUptoLo
	}.property('texto'),
	allTogether: function(){
		return toPass2(this.texto, " ", true, true, true) //tx, sim, rand, elite, toUptoLo
	}.property('texto'),
	tuestilo: function(){
		simbol = this.currentSimbols;
		randmix = simbol == "undefined" || simbol == "r"  ? true : false;
		if (first_time) {
			first_time = false;
		}else{
			if ( ll != this.littlebit) {
				new_word = this.littlebit;
				ll = this.littlebit;
			}else{
				new_word = toPass2(this.texto, simbol, randmix, this.elite, this.mM); //tx, sim, rand, elite, toUptoLo
			}
		}

		return new_word;
	}.property('currentSimbols','texto', 'elite', 'mM', 'mixx', 'littlebit'),
	actions: {
		moreOpcion: function() {
			words = this.filterProperty('txt')
			nWords = words.length;
			slice_words = words.slice(this.min,this.max);
		
			newpass = slice_words.map(function(w){
    			return w.get('txt');
			}).join(" ");
			
			if ( this.max > nWords - this.words_number){
				this.set('min', 0);
				this.set('max', this.words_number);
				this.store.find('word');
				// console.log(this.min + ' entre ' +this.max);
				
			}else{
				
				this.set('min', this.max);
				this.set('max', this.max + this.words_number);
			}
		
			first_time = true;
			this.set('texto', newpass);
			
		},
		focusIn: function() {
			$('input[type=text]').on("click", function () {
			   $(this).select();
			});
			//this.$('input-pass').select();
			// if ( this.texto == ejemplo){
			// 	//this.set('texto', "");
			// }
      	},
   //    	focusOut: function() {
			// if ( this.texto == ""){
			// 	//this.set('texto', ejemplo);
			// }

   //    	},
      			
	},
});


App.ClickableView = Ember.View.extend({
  click: function(evt) {
  	simb = evt.currentTarget.attributes[0].nodeValue;
  	simb = simb == '0' ? "" : simb;
  	this.set('controller.currentSimbols', simb);
  	this.set('currentSimbols', simb);
  	simb == 'r' ? this.set('controller.mixx', !this.get('controller.mixx')) : null;
	// console.log(this.currentSimbols);
  }
});


App.RandtView = Ember.View.extend({
  click: function(evt) {
  	text = this.get('controller.texto');

  	var a = text.split(""),
        n = a.length;
        number = false;
        upCase = false;
        loCase = false;
        simbol = false;
    // console.log(a);

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        /*var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;*/
        l = a[j];
		//console.log((/^[aeio]$/i).test(l) && number);
        if ( !number && (/^[aeio]$/i).test(l) ){
        	a[j] = equi[l.toLowerCase()];
        	number = true;
        }else if (!upCase && (/^[b-df-hj-np-v-z]$/i).test(l) && (l != l.toUpperCase())){
        	a[j] = l.toUpperCase()
        	upCase = true
        }else if (!loCase && (/^[b-df-hj-np-v-z]$/i).test(l) && (l != l.toLowerCase())){
        	a[j] = l.toLowerCase()
        	loCase = true
        }else if(!simbol && l == " "){
        	a[j] = randomSim();
        	simbol = true; 
        }

        if ( number && upCase &&  loCase && simbol  ){
        	 break;
        }        
    } 

    text =  a.join("");
    this.set('controller.littlebit', text);
  }
});



